<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function home(Request $request)
    {
        return view('home');
    }

    public function faq(Request $request)
    {
        return view('faq');
    }
}
