<?php

namespace App\Http\Middleware;

use App\Models\Setting;
use Closure;

class GetSetting
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $asset_version = Setting::byKey('asset_version')->first();
        $logo = Setting::byKey('logo')->first();

        $request->attributes->add([
            'asset_version' => $asset_version ? $asset_version->value : '',
            'logo' => $logo ? $logo->value : '',
        ]);

        return $next($request);
    }
}
