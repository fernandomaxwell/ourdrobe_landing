<?php

namespace App\Http\Middleware;

use App\Models\Menu;
use Closure;

class GetMenu
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $menus = Menu::filterActive()
            ->parentOnly()
            ->needLogin(false)
            ->byLanguage(\App::getLocale())
            ->with('children')
            ->get();

        $request->attributes->add(['menus' => $menus]);

        return $next($request);
    }
}
