<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    /**
     * Guarded fields
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Accessor
     */
    public function getNameAttribute($value)
    {
        return ucwords($value);
    }

    /**
     * Scope filter active
     *
     * @param query $query
     * @return query
     */
    public function scopeFilterActive($query)
    {
        return $query->where('status', 'active');
    }

    /**
     * Scope parent only
     *
     * @param query $query
     * @return query
     */
    public function scopeParentOnly($query)
    {
        return $query->whereNull('parent_id');
    }

    /**
     * Scope by language
     *
     * @param query $query
     * @param String $language
     * @return query
     */
    public function scopeByLanguage($query, $language)
    {
        return $query->where('lang_code', $language);
    }

    /**
     * Scope Need Login
     * 
     * @param query $query
     * @param bool $status
     * @return $query
     */
    public function scopeNeedLogin($query, $status)
    {
        return $query->where('need_login', $status);
    }

    /**
     * Relation
     */
    public function children()
    {
        return $this->hasMany('App\Models\Menu', 'parent_id');
    }
}
