<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    /**
     * Guarded fields
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Scope by key
     *
     * @param query $query
     * @param String $key
     * @return query
     */
    public function scopeByKey($query, $key)
    {
        return $query->where('key', $key);
    }
}
