## How to Install
- git clone
- Copy .env.example to .env, set it your own
- composer install
- php artisan key:generate
- php artisan session:table
- php artisan storage:link
- php artisan migrate
- php artisan db:seed

## How to Deploy
- git pull
- composer install
- composer dump-autoload
- php artisan migrate
- php artisan config:cache (run every deploy)

## Tips to Improve Laravel Performance
- php artisan config:cache
- php artisan route:cache

## How to create new menu (need login)
- add menu into MenuSeeder.php
- add menu url into RoleMenuSeeder.php
- create controller and all function needed for new menu
- add new menu route
- php artisan db:seed