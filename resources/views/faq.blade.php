@extends('layout.master')

@section('css')
    <style>
        .about-group-top {
            background: url("{{ asset('img/background/bg-about-banner.png') }}");
            background-position: right top;
            background-repeat: no-repeat;
        }

        .about-banner {
            height: 100vh;
        }

        .about-vision-mision {
            height: 100vh;
        }

        .about-group-center {
            background: url("{{ asset('img/background/Group 48.png') }}");
            background-position: right bottom;
            background-repeat: no-repeat;
        }

        .about-faq {
            background: url("{{ asset('img/background/bg-about-faq.png') }}");
            background-repeat: no-repeat;
            background-size: cover;
            min-height: 230vh;
        }

        .about-download {
            height: 120vh;
        }

        /* col */
        @media (max-width: 960px) {
            .about-group-top {
                background-position: center;
                background-size: cover;
            }

            .about-banner h1,
            .about-banner h2 {
                color: #FFFFFF;
            }

            .about-group-center {
                background: none;
            }
        }

        /* col-md */
        @media (max-width: 768px) {
            .about-group-top {
                background-position: center;
                background-size: cover;
            }

            .about-banner h1,
            .about-banner h2 {
                color: #FFFFFF;
            }

            .about-group-center {
                background: none;
            }
        }

        /* col-sm */
        @media (max-width: 576px) {
            .about-group-top {
                background-position: center;
                background-size: cover;
            }

            .about-banner h1,
            .about-banner h2 {
                color: #FFFFFF;
            }

            .about-group-center {
                background: none;
            }
        }
    </style>
@endsection

@section('content')
    {{-- Navbar --}}
    <nav class="navbar navbar-expand-md fixed-top navbar-dark py-lg-3">
        {{-- Hamburger Icon --}}
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class='container'>
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                {{-- Logo --}}
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ asset(request()->get('logo')) }}" class='logo' alt="Logo">
                </a>

                {{-- Menu --}}
                <ul class='navbar-nav ml-auto'>
                    @if(request()->get('menus') !== null && !request()->get('menus')->isEmpty())
                        @foreach(request()->get('menus') as $menu)
                            <li class="nav-item my-auto ml-4">
                                @if($menu->is_custom)
                                    <button type='button' class='btn btn-ourdrobe btn-ourdrobe-blue shadow'>
                                        <a class="nav-link py-1 px-4" href="{{ url('/') . $menu->link }}">
                                            <span class='font-weight-bold text-white'>{{ $menu->name }}</span>
                                        </a>
                                    </button>
                                @else
                                    <a class="nav-link" href="{{ url('/') . $menu->link }}">
                                        <span class='font-weight-bold'>{{ $menu->name }}</span>
                                    </a>
                                @endif
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    <div id='about-us' class='about-group-top'>
        {{-- Banner --}}
        <div class='container-fluid about-banner'>
            <div class='container h-100'>
                <div class='row d-flex h-100'>
                    <div class='col-lg-6 align-self-center'>
                        {{-- Title --}}
                        <h1 class='text-ourdrobe-green'>Get Your <span class='font-weight-bold'>Fashionable</span></h1>

                        {{-- Sub Title --}}
                        <h2 class='text-ourdrobe-pink font-weight-bold'>Outfit Here At Ourdrobe</h2>

                        {{-- Body --}}
                        <p class='h4 mt-3'>Ourdrobe always try to innovate to fulfill the needs of the users, try to provide the easiest way to buy preloved clothes, the trusted way to verified the seller, and the safest way to transaction inside Ourdrobe platform.</p>
                    </div>
                </div>
            </div>
        </div>

        {{-- Visi Misi --}}
        <div class='container-fluid about-vision-mision'>
            <div class='container h-100'>
                <div class='row d-flex h-100'>
                    <div class='col-lg-12 align-self-center'>
                        <div class='row'>
                            <div class='col-lg-6'>
                                <img src="{{ asset('img/icon/vision.png') }}" class='w-100'>
                            </div>
                            <div class='col-lg-6'>
                                <img src="{{ asset('img/icon/mision.png') }}" class='w-100'>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class='about-group-center'>
        {{-- FAQ --}}
        <div id='faq' class='container-fluid about-faq text-white pt-5'>
            <div class='container pt-5'>
                {{-- Title --}}
                <h2 class='font-weight-bold text-center pt-5 mb-5'>Frequently Asked Questions</h2>

                {{-- Body --}}
                <div class='my-5'>
                    <h4 class='font-weight-bold'>I’m a new visitor to Ourdrobe. How do I login?</h4>
                    <p class='h5 mt-3'>As you explore Ourdrobe web sites you may encounter content that is only accessible to Ourdrobe members. Should you encounter this type of content, a login screen displays and you need to create an account. Upon completing the registration process you will be able to login using the email and password you entered during account creation. For return visits enter your Email and Password in the login box.</p>
                </div>
                <div class='my-5'>
                    <h4 class='font-weight-bold'>I can't login to my account.</h4>
                    <p class='h5 mt-3'>If you can't access your account with the email you signed up with:
                        Open the Ourdrobe app on your mobile device and enter your last known email address.
                        Tap Forgot password?.
                        Follow the on-screen instructions to submit a support request.
                    </p>
                </div>
                <div class='my-5'>
                    <h4 class='font-weight-bold'>Why does my  account say error when I try to log in?</h4>
                    <p class='h5 mt-3'>Typically, the account login error comes up anytime you try to log into your account on the mobile app whether android or ios. The problem might either be caused by your device or some technical fault in the Ourdrobe service.</p>
                </div>
                <div class='my-5'>
                    <h4 class='font-weight-bold'>Lorem ipsum dolor sit amet, consectetur adipiscing?</h4>
                    <p class='h5 mt-3'>Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit. Amet, consectetur adipis Lorem ipsum dolor sit amet, consectetur adipiscing elit. cing elit.</p>
                </div>
                <div class='my-5'>
                    <h4 class='font-weight-bold'>What types of card can be used for making payment? </h4>
                    <p class='h5 mt-3'>We are accepting all credit cards (embossed visa & mastercard) & QRIS (GOPAY, OVO, Shopeepay, DANA, etc.). All credit card payment transactions must be authorised by the cardholder's bank.</p>
                </div>
                <div class='my-5'>
                    <h4 class='font-weight-bold'>What are expeditions that I can used in Ourdrobe? </h4>
                    <p class='h5 mt-3'>For now, we are only available on JNE & Tiki. Soon we will expand and add more only for you.</p>
                </div>
            </div>
        </div>

        {{-- Download --}}
        <div class='container-fluid about-download'>
            <div class='container'>
                <div class='row'>
                    <div class='col-lg-5 mt-5'>
                        <div class='row mt-5'>
                            <div class='col-lg-12 mt-5'>
                                <p class='h3'>Are you still considering <br><span class='h2'>to use Ourdrobe</span> <br>as your <span class='font-weight-bold'>shopping buddy?</span> download and feel <br><span class='float-right'>the <span class='h2'>experience!</span></span></p>
                            </div>

                            {{-- Download --}}
                            <div class='col-lg-12 mt-5 text-right'>
                                <a href='#'>
                                    <img src="{{ asset('img/icon/app-store.png') }}">
                                </a>
                                <a href='#'>
                                    <img src="{{ asset('img/icon/gplay-store.png') }}">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id='contact-us' class='container-fluid contact-us'>
        {{-- Contact --}}
        @include('layout.contact')

        {{-- Footer --}}
        @include('layout.footer-self')
    </div>
@endsection

@section('javascript')
    <script>
        window.onscroll = function() {scrollFunction()}
        window.onload = function() {scrollFunction()}
        window.onresize = function() {scrollFunction()}

        const scrollFunction = () => {
            if (document.body.scrollTop > 10 || document.documentElement.scrollTop > 10 || $(window).innerWidth() <= 960) {
                $('.fixed-top').removeClass('navbar-dark py-lg-3').addClass('bg-white navbar-light shadow py-lg-0')
            } else {
                $('.fixed-top').removeClass('bg-white navbar-light shadow py-lg-0').addClass('navbar-dark py-lg-3')
            }
        }

        $('.footer-faq').on('click', function (e) {
            e.preventDefault()

            $('html, body').animate({
                scrollTop: $($(this).attr('href')).offset().top,
            }, 300, 'linear')
        })
    </script>
@endsection