<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>- {{ config('app.name') }}</title>

        {{-- Font --}}
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet" type='text/css'>

        {{-- CSS --}}
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/custom.css') }}?v={{ request()->get('asset_version') }}" rel="stylesheet">

        {{-- Icon --}}
        <link href="{{ asset('img/icon-green.png') }}?v={{ request()->get('asset_version') }}" rel="icon">

        @yield('css')
    <head>

    <body>
        @yield('content')

        {{-- Bootstrap core JavaScript --}}
        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>

        {{-- Core plugin JavaScript --}}
        <script src="{{ asset('js/jquery.easing.min.js') }}"></script>

        @yield('javascript')
    </body>
</html>