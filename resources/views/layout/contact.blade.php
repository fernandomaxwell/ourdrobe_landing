<style>
    .contact-us {
        background: url("{{ asset('img/background/bg-contact-us.png') }}") center;
        background-position: center -15%;
        background-repeat: no-repeat;
        background-size: cover;
    }
</style>

<div class='container'>
    <div class='row'>
        {{-- Left --}}
        <div class='col-lg-5'>
            {{-- Title --}}
            <img src="{{ asset('img/text/contact-us.png') }}">

            {{-- Body --}}
            <p class='h4 my-3'>Any business inquiry? or do you want to collaborate with Ourdrobe? drop a message here!</p>

            {{-- Form --}}
            <form>
                <div class="form-group">
                    <input type="text" class="form-control form-control-lg" placeholder="Name">
                </div>
                <div class="form-group my-4">
                    <input type="email" class="form-control form-control-lg" placeholder="Email">
                </div>
                <div class="form-group">
                    <textarea class="form-control" rows="5"></textarea>
                </div>
                <button type='button' class='btn btn-ourdrobe btn-ourdrobe-green float-right px-5'>
                    <span class='font-weight-bold'>Send</span>
                </button>
            </form>
        </div>

        <div class='col-lg-2'></div>

        {{-- Right --}}
        <div class='col-lg-5 text-white mb-2'>
            <div class='row d-flex h-100'>
                <div class='col-lg-12 align-self-end'>
                    <img src="{{ asset(request()->get('logo')) }}" class='logo' alt="Logo">
                    <p class='h5 mt-3 mb-5'>Ourdrobe is a online marketplace who focus on connecting the thrift shop owner & buyer. Ourdrobe have 3 pillars which are trusted seller, user friendly, and safe transaction.</p>

                    {{-- Social Media --}}
                    <a href='https://www.instagram.com/ourdrobe_id/' target='_blank' class='text-white'>
                        <img src="{{ asset('img/icon/ig.png') }}">
                        <span class='h5 font-weight-bold'>@ourdobe_id</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>