<div class='container text-white footer'>
    <div class='row'>
        <div class='col-lg-5 offset-lg-7 my-4'>
            <p>
                <span>&copy; {{ date ('Y') }} {{ config('app.name') }}. Powered By DNA</span>
                <span class='float-right'>
                    <span class='mr-4 font-weight-bold'>
                        <a href="{{ url('/') }}#about-us" class='text-white footer-about-us'>About Us</a>
                    </span>
                    <span class='font-weight-bold'>
                        <a href="#faq" class='text-white footer-faq'>FAQ</a>
                    </span>
                </span>
            </p>
        </div>
    </div>
</div>