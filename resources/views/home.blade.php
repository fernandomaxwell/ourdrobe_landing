@extends('layout.master')

@section('css')
    <style>
        .home-banner {
            background: url("{{ asset('img/background/bg-home-banner.png') }}");
            background-position: 0px -50px;
            background-repeat: no-repeat;
            background-size: cover;
            height: 125vh;
        }

        .home-about {
            background: url("{{ asset('img/background/bg-home-about-us.png') }}");
            background-position: -200px 0%;
            background-repeat: no-repeat;
            background-size: cover;
            height: 120vh;
        }

        .home-what-we-do {
            background: url("{{ asset('img/background/bg-home-what-we-do.png') }}");
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        .home-what-we-do-bottom {
            background: url("{{ asset('img/background/bg-home-what-we-do-bottom-right.png') }}"),
                url("{{ asset('img/background/bg-home-what-we-do-bottom.png') }}");
            background-position: 140% top, 35% 0%;
            background-repeat: no-repeat;
            background-size: contain, cover;
            height: 120vh
        }

        .home-what-we-do-top{
            height: 100vh;
        }

        /* col-lg */
        @media (min-width: 960px) {
            .home-what-we-do-bottom {
                color: #FFFFFF;
            }
        }

        @media (max-width: 960px) {
            .home-banner {
                background: url("{{ asset('img/background/bg-home-banner-responsive.png') }}");
                background-position: center;
                height: 110vh;
            }

            .home-about {
                height:100vh;
            }

            .home-what-we-do-top{
                height: auto;
            }

            .home-what-we-do-bottom {
                background: url("{{ asset('img/background/bg-home-what-we-do-bottom.png') }}");
                background-position: 35% 0%;
                background-repeat: no-repeat;
                background-size: cover;
            }
        }

        /* col-md */
        @media (max-width: 768px) {
            .home-banner {
                background: url("{{ asset('img/background/bg-home-banner-responsive.png') }}");
                background-position: center;
                height: 110vh;
            }

            .home-about {
                height:100vh;
            }

            .home-what-we-do-top{
                height: auto;
            }

            .home-what-we-do-bottom {
                background: url("{{ asset('img/background/bg-home-what-we-do-bottom.png') }}");
                background-position: 35% 0%;
                background-repeat: no-repeat;
                background-size: cover;
            }
        }

        /* col-sm */
        @media (max-width: 576px) {
            .home-banner {
                background: url("{{ asset('img/background/bg-home-banner-responsive.png') }}");
                background-position: center;
                height: 110vh;
            }

            .home-about {
                height:100vh;
            }

            .home-what-we-do-top{
                height: auto;
            }

            .home-what-we-do-bottom {
                background: url("{{ asset('img/background/bg-home-what-we-do-bottom.png') }}");
                background-position: 35% 0%;
                background-repeat: no-repeat;
                background-size: cover;
            }
        }
    </style>
@endsection

@section('content')
    {{-- Navbar --}}
    <nav class="navbar navbar-expand-lg fixed-top navbar-dark py-lg-3">
        {{-- Hamburger Icon --}}
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class='container'>
            <div class="collapse navbar-collapse" id="collapsibleNavbar">
                {{-- Logo --}}
                <a class="navbar-brand" href="{{ url('/') }}">
                    <img src="{{ asset(request()->get('logo')) }}" class='logo' alt="Logo">
                </a>

                {{-- Menu --}}
                <ul class='navbar-nav ml-auto'>
                    @if(request()->get('menus') !== null && !request()->get('menus')->isEmpty())
                        @foreach(request()->get('menus') as $menu)
                            <li class="nav-item my-auto ml-4">
                                @if($menu->is_custom)
                                    <button type='button' class='btn btn-ourdrobe btn-ourdrobe-blue shadow'>
                                        <a class="nav-link py-1 px-4" href="{{ $menu->link }}">
                                            <span class='font-weight-bold text-white'>{{ $menu->name }}</span>
                                        </a>
                                    </button>
                                @else
                                    <a class="nav-link" href="{{ $menu->link }}">
                                        <span class='font-weight-bold'>{{ $menu->name }}</span>
                                    </a>
                                @endif
                            </li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    <div class='home-group-top'>
        {{-- Banner --}}
        <div class='container-fluid home-banner'>
            <div class='container h-100'>
                <div class='row d-flex h-100'>
                    <div class='col-lg-6 d-none d-lg-block'></div>

                    <div class='col-lg-6 align-self-center text-white'>
                        {{-- Title --}}
                        <h1 class='text-uppercase font-weight-bold'>
                            Sell, shop <div>and style</div>
                        </h1>

                        {{-- Line --}}
                        <div class="hl mt-2 mb-3"></div>

                        {{-- Body --}}
                        <p class='h3'>The easiest way to found the best thrift outfit for you</p>
                        
                        {{-- Download --}}
                        <div class='mt-5'>
                            <a href='#'>
                                <img src="{{ asset('img/icon/app-store.png') }}">
                            </a>
                            <a href='#'>
                                <img src="{{ asset('img/icon/gplay-store.png') }}">
                            </a>
                        </div>

                        {{-- Spacer --}}
                        <div class='d-none d-lg-block' style='height:150px'></div>
                    </div>
                </div>
            </div>
        </div>

        {{-- About --}}
        <div id='about-us' class='container-fluid home-about'>
            <div class='container h-100'>
                <div class='row d-flex h-100'>
                    <div class='col-lg-6 align-self-center'>
                        {{-- Title --}}
                        <img src="{{ asset('img/text/home-about-us.png') }}">

                        {{-- Body --}}
                        <p class='h4 mt-3 mb-5'>Ourdrobe is a online marketplace who focus on connecting the thrift shop owner & buyer. Ourdrobe have 3 pillars which are trusted seller, user friendly, and safe transaction.</p>

                        {{-- Read More --}}
                        <button type='button' class='btn btn-ourdrobe btn-ourdrobe-green px-4'>
                            <a href="{{ url('faq') }}" class='font-weight-bold text-white'>Read more</a>
                        </button>

                        {{-- Spacer --}}
                        <div class='d-none d-lg-block' style='height:200px'></div>
                    </div>
                </div>
            </div>
        </div>

        {{-- What We Do --}}
        <div class='home-what-we-do'>
            {{-- Top --}}
            <div id='benefit' class='container-fluid home-what-we-do-top'>
                <div class='container h-100'>
                    <div class='row d-flex h-100'>
                        <div class='col-lg-12 align-self-end text-white mt-5 mt-lg-0'>
                            <div class='row mt-5 mt-lg-0'>
                                <div class='col-lg-8 offset-lg-2 text-center mt-5 mt-lg-0'>
                                    <h1 class='font-weight-bold mt-5 mt-lg-0 mb-4'>What We do?</h1>
                                    <p class='h4'>Ourdrobe platform connect thrift shop owner and buyer to experience the new way of thrift shopping.</p>
                                </div>
                            </div>

                            <div class='row mt-4'>
                                <div class='col-lg-4 mt-4'>
                                    <div class='card border-0 bg-transparent'>
                                        <div class='card-body text-center px-0'>
                                            <img src="{{ asset('img/icon/user-friendly.png') }}" class='mb-5'>
                                            <p class='h4 font-weight-bold'>User friendly</p>
                                            <!--<p class='h4'>Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, consectetur</p>   -->
                                        </div>
                                    </div>
                                </div>

                                <div class='col-lg-4 mt-4'>
                                    <div class='card border-0 bg-transparent'>
                                        <div class='card-body text-center px-0'>
                                            <img src="{{ asset('img/icon/trusted-seller.png') }}" class='mb-5'>
                                            <p class='h4 font-weight-bold'>Trusted seller</p>
                                            <!--<p class='h4'>Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, consectetur</p>   -->
                                        </div>
                                    </div>
                                </div>

                                <div class='col-lg-4 mt-4'>
                                    <div class='card border-0 bg-transparent'>
                                        <div class='card-body text-center px-0'>
                                            <img src="{{ asset('img/icon/safe-transaction.png') }}" class='mb-5'>
                                            <p class='h4 font-weight-bold'>Safe transaction</p>
                                            <!--<p class='h4'>Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet, consectetur</p>   -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Spacer --}}
            <div class='d-none d-lg-block' style='height:100px'></div>

            {{-- Bottom --}}
            <div class='container-fluid home-what-we-do-bottom'>
                <div class='container h-100'>
                    <div class='row h-100'>
                        <div class='col-lg-12 align-self-center'>
                            <div class='row'>
                                <div class='col-lg-4 offset-lg-8'>
                                    <p class='h3'>You have <span class='font-weight-bold'>preloved clothes</span> in your house? sell to <span class='h2 font-weight-bold'>Ourdrobe!</span></p>

                                    {{-- Download --}}
                                    <div class='mt-3 text-right'>
                                        <a href='#'>
                                            <img src="{{ asset('img/icon/app-store.png') }}">
                                        </a>
                                        <a href='#'>
                                            <img src="{{ asset('img/icon/gplay-store.png') }}">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Spacer --}}
        <div style='height:150px'></div>

        <div id='contact-us' class='container-fluid contact-us'>
            {{-- Contact --}}
            @include('layout.contact')

            {{-- Footer --}}
            @include('layout.footer-general')
        </div>
    </div>
@endsection

@section('javascript')
    <script>
        window.onscroll = function() {scrollFunction()}
        window.onload = function() {scrollFunction()}
        window.onresize = function() {scrollFunction()}

        const scrollFunction = () => {
            if (document.body.scrollTop > 10 || document.documentElement.scrollTop > 10 || $(window).innerWidth() <= 960) {
                $('.fixed-top').removeClass('navbar-dark py-lg-3').addClass('bg-white navbar-light shadow py-lg-0')
            } else {
                $('.fixed-top').removeClass('bg-white navbar-light shadow py-lg-0').addClass('navbar-dark py-lg-3')
            }

            let scrollPos = $(document).scrollTop()
            $('.navbar-nav a').each(function () {
                let currLink = $(this)
                let refElement = $(currLink.attr('href'))
                let adjustmentHeight = 200
                if ((refElement.position().top - adjustmentHeight) <= scrollPos && (refElement.position().top - adjustmentHeight) + refElement.height() > scrollPos) {
                    $('.navbar-nav li a').removeClass('active')
                    currLink.addClass('active')
                } else {
                    currLink.removeClass('active')
                }
            });
        }

        $('.fixed-top a[href*="#"], .footer-about-us').on('click', function (e) {
            e.preventDefault()

            $('html, body').animate({
                scrollTop: $($(this).attr('href')).offset().top,
            }, 300, 'linear')
        })
    </script>
@endsection